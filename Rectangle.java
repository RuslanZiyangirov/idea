package com.company;

import java.util.Scanner;

public class Rectangle extends Figure {
    @Override
    double getSquare() {
        Scanner scanner = new Scanner(System.in);
        int side1 = scanner.nextInt();
        int side2 = scanner.nextInt();
        int square = side1*side2;
        return  square;
    }
}
