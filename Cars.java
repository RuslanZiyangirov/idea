package com.company;

public class Cars {

    public int carnumber = 1000;
    public int trucknumber = 9999;



    public int getCarNumber() {
        return carnumber;
    }

    public void setCarNumber(int carnumber) {
        this.carnumber = carnumber;
    }

    public int getTruckNumber(){
        return trucknumber;
    }

    public void setTruckNumber(int trucknumber){
        this.trucknumber = trucknumber;
    }

    public int truckNumberGeneration(){
        setTruckNumber(getTruckNumber()-1);
        return getTruckNumber();
    }

    public int carNumberGeneration(){
        setCarNumber(getCarNumber()+1);
        return getCarNumber();
    }


}




