package com.company;

import java.util.Scanner;

public class Circle extends Figure{
    @Override
    double getSquare() {
        Scanner scanner = new Scanner(System.in);
        int radius = scanner.nextInt();
        double square = Math.PI*radius*radius;
        return square;
    }
}
