package com.company;

import java.util.ArrayList;

public class Parking {
    Cars cars;
    Data data;
    int places;
    int newTrucks;
    int newCars;
    int n;
    public ArrayList<Integer> arrParkingForNumber;
    public ArrayList<Integer> arrParkingForStep;

    public Parking(int places, int n) {
        cars = new Cars();
        this.places = places;
        data = new Data();
        this.n = n;
        arrParkingForNumber = new ArrayList<>(places);
        arrParkingForStep = new ArrayList<>(places);
        for (int i = 0; i < places; i++) {
            arrParkingForNumber.add(0);
        }
        for (int i = 0; i < places;i++){
            arrParkingForStep.add(0);
        }
    }

    /*public void ArrayParkingForNumber(){ // создал "парковку" для номеров и обнулии все места
        for (int i =0 ; i < arrParkingForNumber.size(); i++) {
            arrParkingForNumber.add(0);
        }


    }

    public void ArrayParkingForStep (){ // создал "парковку" для ходов и обнулили все места
        for (int i=0; i < arrParkingForStep.size(); i++) {
            arrParkingForStep.add(0);
        }
    }*/

    public void showFreePlacesTheNumberOfSteps() {
        int k = 0;
        for (int i = 0; i < arrParkingForStep.size(); i++) {
            if (arrParkingForStep.get(i) == 0) {
                k = k + 1;
            }

        }
        System.out.println("Мест свободно: " + k);
    }

    public void showBusyPlacesTheNumberOfSteps() {
        int p = 0;
        for (int i = 0 ; i < arrParkingForStep.size(); i++) {
            if (arrParkingForStep.get(i) != 0) {
                p = p + 1;
            }
        }
        System.out.println("Мест занято: " + p);
    }


    public int newCreateCars() { // бесконечное создание новых машин
        newCars = (int) (Math.random() * places / 3) + 1;
        return newCars;
    }

    public int newCreateTrucks() { // бесконечное создание новых грузовиков
        newTrucks = (int) (Math.random() * places / 3) + 1;
        return newTrucks;
    }

    public void initializationSteps() {
        for (int i = 0; i < arrParkingForStep.size(); i++) {
            if (arrParkingForStep.get(i) == 0) {
                arrParkingForNumber.set(i,0);

            }
        }
    }

    public int nearestPlace() {
        int min = 2147483647;
        for (int j = 0; j < arrParkingForStep.size(); j++) {
            if (arrParkingForStep.get(j) < min) {
                min = arrParkingForStep.get(j);
            }
        }
        return min;
    }

    public void cleanParking(){
        for (int i = 0; i < arrParkingForNumber.size(); i++) {
            arrParkingForNumber.set(i,0);
        }
        for (int i = 0; i < arrParkingForStep.size(); i++) {
            arrParkingForStep.set(i,0);
        }
    }

    public void type(){
        for (int i = 0; i < arrParkingForNumber.size(); i++) {
            if (i < n ){
                System.out.println((i+1)+") Место - Тип: Для машины ");
            }else{
                System.out.println((i+1)+") Место - Тип: Для грузовика ");
            }
        }
    }

    public void numberOfCarsAndTrucks(){
        for (int i = 0; i < arrParkingForNumber.size(); i++) {
            if (i < n ){
                System.out.println((i+1)+") Место - Номер машины: " + arrParkingForNumber.get(i));
            }else{
                System.out.println((i+1)+") Место - Номер грузовика: " + arrParkingForNumber.get(i));
            }
        }
    }

    public void lifeTime(){
        for (int i = 0; i < arrParkingForStep.size(); i++) {
             if (i < n ){
                System.out.println((i+1)+") Место - Оставшееся время жизни: " + arrParkingForStep.get(i));
             }else{
                System.out.println((i+1)+") Место - Оставшееся время жизни: " + arrParkingForStep.get(i));
             }
        }

    }

    public void entryOfCarsAndTrucks() {
        for (int i = 0; i < arrParkingForNumber.size(); i++) {
            if (arrParkingForNumber.get(i) == 0 && i < n && newCars>0 ) {
                arrParkingForNumber.set(i,cars.carNumberGeneration());
                arrParkingForStep.set(i,data.randomTimeStep());
                newCars = newCars -1;

            } else if (arrParkingForNumber.get(i) == 0 && i >= n && newTrucks>0) {
                arrParkingForNumber.set(i,cars.truckNumberGeneration());
                arrParkingForStep.set(i,data.randomTimeStep());
                newTrucks = newTrucks -1;
            } else if (arrParkingForNumber.get(i) != 0 && i >= n &&
                    (arrParkingForNumber.get(i) == 0 && arrParkingForNumber.get(i+1) == 0 && i < n) && newTrucks>0) {
                arrParkingForNumber.set(i,cars.truckNumberGeneration());
                arrParkingForNumber.set(i+1,cars.getTruckNumber());
                arrParkingForStep.set(i,data.randomTimeStep());
                arrParkingForStep.set(i+1,data.getTimeStep()) ;
                newTrucks = newTrucks -1;
            }
        }
        fullPlaces();
    }

    public void minusOneStep() {
        for (int i = 0; i < arrParkingForStep.size() ; i++) {
            if (arrParkingForStep.get(i) !=0) {
                arrParkingForStep.set(i,arrParkingForStep.get(i) - 1);;
            }

        }
    }

    public void fullPlaces(){
        int k = 0;
        for (int i = 0; i < arrParkingForNumber.size(); i++) {
            if( arrParkingForNumber.get(i) != 0){
                k = k+1;
            }

        }
        if (k == places){
            System.out.println(" \n" +
                    "Все места на парковке заняты");
        }

    }


}