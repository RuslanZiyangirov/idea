package com.company;

public class Data {

    public int timestep;

    public int getTimeStep() {
        return timestep;
    }

    public void setTimeStep(int timestep) {
        this.timestep = timestep;
    }

    public int randomTimeStep() {
        timestep = (int) (Math.random() * 10) + 1;
        return timestep;
    }

}
