package com.company;

import java.util.Scanner;

public class Square extends Figure {
    @Override
    double getSquare() {
        Scanner scanner = new Scanner(System.in);
        int side = scanner.nextInt();
        int square = side*side;

        return square;
    }
}
