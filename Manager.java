package com.company;

import java.util.Scanner;
import java.util.InputMismatchException;

public class Manager {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (true) {

                System.out.println("Введите кол-во мест на парковке: ");
                int a = scan.nextInt();
                System.out.println("Введите кол-во мест для машин,оставшаяся часть будет для грузовиков");
                int n = scan.nextInt();
                while (n > a) {
                    System.out.println("Неправильное распределение мест,повторите заново, пожалуйста");
                    n = scan.nextInt();
                }
                Parking parking = new Parking(a, n);
                Data data = new Data();


                while (true) {
                    System.out.println(" \n" +
                            "1) Перейти к след.ходу. \n" +
                            "2)Узнать сколько мест занято. \n" +
                            "3)Узнать сколько мест свободно \n" +
                            "4)Когда освободится ближайшее \n" +
                            "5)Очистить парковку от всех машин \n" +
                            "6)Тип парковочного места \n" +
                            "7)Номер машин/грузовиков \n" +
                            "8)Оставшиеся время 'жизни' машин/грузовиков \n");

                    String number = scan.next();
                    switch (number) {
                        case ("1")://ход
                            parking.minusOneStep();
                            parking.initializationSteps();
                            parking.newCreateCars();
                            parking.newCreateTrucks();
                            parking.entryOfCarsAndTrucks();
                            break;

                        case ("2"):
                            parking.showBusyPlacesTheNumberOfSteps();
                            break;

                        case ("3"):
                            parking.showFreePlacesTheNumberOfSteps();
                            break;

                        case ("4"):
                            System.out.println("Ближайшее место освободится через " + parking.nearestPlace() + " ходов");
                            break;

                        case ("5"):
                            parking.cleanParking();
                            break;

                        case ("6"):
                            parking.type();
                            break;
                        case ("7"):
                            parking.numberOfCarsAndTrucks();
                            break;
                        case ("8"):
                            parking.lifeTime();
                            break;

                        default:
                            System.out.println("Неправильный ввод,пожалуйста,введите цифру из указаннного набора!");

                            break;
                    }
                }  
        }
    }
}